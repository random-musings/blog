---
title: "Lemon rice"
date: 2021-07-22T06:37:27+05:30
---

# Time

1. Preparation - 1 min
2. Boiling rice - 20 mins
3. Main dish - 5 to 7 mins

# Ingredients

- Groundnut oil - 1 Tbsp

- Mustard - 1/4 Tsp

- Urud dhal - 1 Tsp
- Channa dhal - 1 Tsp
- Ground nut - 2 Tsp (optional)
- Cashew - 5 nos (optional)

- Turmeric powder - 1/4 Tsp
- Asofoetida - 1 pinch
- Red chilli - 3
- Green chilli - 3 (optional)
- Curry leaves - 5 leaves

- Lemon juice - Juice from 1.5 lemon (medium size)
- Salt - To taste

- Rice - 1 cup or 100 grams or 3/4 tumbler
- Water - 1:3 (For sona masoori rice)

- Coriander - 10 leaves

## Preparation

1. Finely chop the green chilli (optional).
2. Remove the head of red chilli.
3. Wash the coriander and curry leaves.

## Cooking

### Boiling rice

1. In a cooker, add rice and water in 1:3 ratio and cook for 3 whistles.
2. Keep the flame high until 1st whistle and change to medium after that.

### Main dish

1. Heat oil in a pan, add mustard, asfoetida and fry till you hear crackling sound.
2. Add urud dhal, chenna dhal, ground nut, cashew nuts and fry till light golden brown.
3. Add chopped green chilli, whole red chilli and curry leaves.
4. Add turmeric powder and switch off the stove.
5. After 15 sec add the lemon juice and salt to taste.
6. Add the rice and mix well. Color will change to yellow.
7. Garnish with coriander.

## Insights

1. Adding lemon juice when the stove is on will make the dish bitter.
2. Crushing the lemon very hard will make the dish bitter.
3. If the rice is very sour, add more rice.

## Video links

1. Madras samayal - https://youtu.be/Kkog1jDVS6M
2. Chef Damu - https://youtu.be/FMl9vS_yS4k
