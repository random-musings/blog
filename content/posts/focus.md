---
title: "Focus, distractions and how to handle them"
date: 2021-07-19T02:50:29+05:30

summary: "It is not uncommon to find ourselves stuck in either a past context
or an anticipated future, rather than living in the present moment. In this
blog, I try to explore this scenario and how we can help ourselves through it."

---

```text
- Will I be able to complete this ?
- What should I be doing with my time ?
- When I will complete this in two days... I will be getting....
```


It is not uncommon to find ourselves stuck in either a past context or an
anticipated future, rather than living in the present moment. In this blog, I
try to explore this scenario and how we can help ourselves through it.

## When we say we are distracted, most likely:

1. Our mind is filled disturbing thoughts. For example, feeling of not being
   good enough, feeling of not have acted in a certain way.

2. We are constantly focused on what we will achieve, instead of working with
   what we already know.

3. We are frustrated for not having enough time.

Before learning how we could help ourselves through each of the situations, let's
try to understand what focus is all about.

## What is focus

We humans have the ability to think both backward and forward in time (read past
and future). Focus is about freeing ourselves from these thoughts and giving our
attention to the **UNDEMANDING PRESENT**.

## How do we focus

### Notice your thoughts

First thing is to watch your thoughts as they flow through the mind. As you
watch, try to identify the ones that are disturbing and ones that are not.

1. Disturbing thoughts take your attention either to a moment in past or in an
   anticipated future.

2. Non-disturbing thoughts keep your attention in this moment. This could be the
   thought of an object around you (a bird, a leaf or some specific task in
   front of you).

Out of the many non-disturbing thoughts you notice, pick one of them and focus
your attention on it. Because the mind can focus only on one thing at a time,
the other thoughts (including the disturbing ones) are out of your mind for a
few moments.

### Forget the goal, think about what you can do now

Having chosen *one non-disturbing* thought to focus our attention, now we will
see how to sustain our attention on it.

We live in a performance based society, where we continuously strive towards new
and better instead of focusing our inner power to constructively work on
things what we are best in this moment of time.

Striving towards new and better, actually moves our attention to what we will
achieve, instead of keeping it on the work that needs to be done now.

Removing the goal or thoughts about growth/improvement for a few moments might
be scary in the beginning. But, it is this act that will shift our attention
from *who we are not and what we don't have* to *who we are and what we have*.

Thinking this way, focus is not about becoming something new or something
better, but functioning exactly as we are and understanding this is enough for
general happiness and great achievements.

### Possibilities can be endless, but you can pursue only a few of them

Now, we come back to the third factor that causes distraction: `Being frustrated
about not having enough time`.

We live in time where, there are simply endless possibilities around us. It
makes us believe life must be lived with intensity and we try to excel in all
the arenas around us. This has two downsides:

1. Our attention is shattered across these many different things.
2. Because, we make an attempt to excel in many things, our self confidence is
   also linked to the outcome of each of these things.

Rather, we should be focusing on one thing and do it really well. This helps us
excel in that one thing better and hence improve our self confidence. This
improved self confidence will help us tackle the next moment better.

To sum it up, in the world of endless possibilities, it is not about
prioritizing things, rather it is about de-prioritising things. In other words,
*more possibilities, more there is to refrain from*.

One of the effective ways to put this in practice could be, to maintain a *NOT
TODO* list as you maintain a *TODO* list.

Despite having the ability to think about our long term well-being, we are
increasingly focused on *instantaneous reward* such as expecting a like/comment
on your social media post. By giving us into these short term rewards, we loose
an important ability: to build our own self-esteem without constant feedback
from others.

So how do we find a long-term focus, one that moves our life in the right
direction. Achieving long term focus requires that we learn to direct our
attention inwards. To your core beliefs that doesn't need a constant response.
It is in our inner core that we find *genuine contenment and satifaction* with
who we are.

## Concluding note: Why do we need to do all of these ?

Because, focus is an important thing in our lives. Several essential abilities
are innate skills are linked to this innate skill: the ability to learn, to
listen, to empathize and of course to steer our lives in the right direction.

As the pace our society will increase even more, it will require quite a lot to
navigate in this unpredictable era. With a growing road around us, we should
control the intense of the society and not let the intense society control us.
And for this, we need to stay *sharp and focused*


It will not be surprising to see just two groups of people in the future:
1. Those with the ability to co-exist and handle the intense society.
2. Those who will become more or less slaves under the same possibilities.

Although, we as adults are struggling with our focus more than ever, we still
have a sense of what it is. But about our future generations ?

If we do not practice this skill well enough and pass it on to our future
generations, our kids or probably grand kids may completely loose access to this
life-affirming tool.

## Reference

This is not my original idea. I have written down the ideas presented by
[Christina Bengtsson](https://www.christinabengtsson.com/) in her TEDx talk
[The art of focus - a crucial
ability](https://www.youtube.com/watch?v=xF80HzqvAoA) in the way I understood
it.
